﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/EnemyAttack")]
public class Action_EnemyAttack : AIAction
{
    public float waitTime;
    public float variety;

    public override void Act(EnemyController _controller)
    {
        if (_controller.CheckIfTimeElapsed(waitTime, variety))
        {
            _controller.basicAI.UseEnemySkill();
        }
    }

}
