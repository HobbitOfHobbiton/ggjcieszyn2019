﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    PlayerUI playerUI;

    const int maxHealth = 100;
    int currentHealth;

    private void Start()
    {
        currentHealth = maxHealth;

        playerUI = GetComponentInChildren<PlayerUI>();
    }

    public void TakeDamage(int _value)
    {
        currentHealth -= _value;

        if (currentHealth < 0)
            currentHealth = 0;

        playerUI.ScaleHealthBar(currentHealth, maxHealth);


    }


}
