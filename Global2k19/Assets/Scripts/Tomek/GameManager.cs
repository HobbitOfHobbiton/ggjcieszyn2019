﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public GameUI gameUI;

    Goal goal = Goal.KillEnemies;

    public int enemiesToKill = - 1;
    public delegate bool StageGoal();
    StageGoal stageGoal;


    public int currentStageIndex = 0;
    int maxStageIndex;
    public StageInfo[] stageInfos;
    bool stageDone = true;

    //------------------------------------------------------------------------------------------------------

    void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        gameUI = GetComponentInChildren<GameUI>();
    }

    private void Start()
    {
        maxStageIndex = stageInfos.Length - 1;
        SetupStage();
    }

    private void Update()
    {
        if (stageDone)
            return;

        if (!stageGoal())
        {
            return;
        }

        gameUI.DisableGoals();
        stageDone = true;

        currentStageIndex++;

        if (currentStageIndex > maxStageIndex)
        {
            GameOver(true);
        }
    }

    //------------------------------------------------------------------------------------------------------


    public void SetupStage()
    {
        Debug.Log(currentStageIndex);
        Debug.Log(maxStageIndex);


        StageInfo stageInfo = stageInfos[currentStageIndex];

        goal = stageInfo.goal;
        gameUI.EnableGoals();

        switch (goal)
        {
            case Goal.KillEnemies:
                enemiesToKill = stageInfo.enemiesTokillCount;
                gameUI.UpdateEnemiesToKill(enemiesToKill);
                stageGoal = CheckIfAllEnemiesKilled;
                break;
            case Goal.KillEnemiesUntilTimeEnd:
                enemiesToKill = stageInfo.enemiesTokillCount;
                gameUI.UpdateEnemiesToKill(enemiesToKill);
                stageGoal = CheckIfAllEnemiesKilledInTime;
                timerCor = StartCoroutine(Timer_Cor(stageInfo.timerDuration));
                break;
            default:
                break;
        }

        stageDone = false;
    }



    //------------------------------------------------------------------------------------------------------

    public bool CheckIfAllEnemiesKilled()
    {
        if (enemiesToKill <= 0)
        {
            return true;
        }
        else
            return false;
    }

    public bool CheckIfAllEnemiesKilledInTime()
    {
        if (enemiesToKill <= 0)
        {
            StopCoroutine(timerCor);
            return true;
        }
        else
        {
            return false;
        }
    }

    //------------------------------------------------------------------------------------------------------

    public void KilledEnemy()
    {
        enemiesToKill--;
        gameUI.UpdateEnemiesToKill(enemiesToKill);
    }

    //------------------------------------------------------------------------------------------------------

    Coroutine timerCor;

    IEnumerator Timer_Cor(float _duration)
    {
        for (float time = _duration; time > 0; time -= Time.deltaTime)
        {
            gameUI.UpdateTimeLeft(time);

            yield return null;
        }


        GameOver(false);
        stageDone = true;
    }

    public void GameOver(bool _won)
    {
        gameUI.GameOver(_won);
    }

}

public enum Goal { KillEnemies, KillEnemiesUntilTimeEnd}

public enum Stages { Stage1, Stage2 }
 
