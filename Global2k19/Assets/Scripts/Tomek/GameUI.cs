﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour {

    public Text enemiesToKillText;
    public Text timeLeftText;
    public Text gameOverText;

    [SerializeField] Image avatar1;
    [SerializeField] Image avatar2;
    [SerializeField] Image avatar3;
    [SerializeField] Image cooldownImage;


    public void UpdateEnemiesToKill(int _count)
    {
        enemiesToKillText.text = "Enemies left: " + _count;
    }

    public void UpdateTimeLeft(float _time)
    {
        timeLeftText.text = "Time left: " + _time.ToString("F2");
    }

    public void EnableGoals()
    {
        enemiesToKillText.gameObject.SetActive(true);
        timeLeftText.gameObject.SetActive(true);
    }

    public void DisableGoals()
    {
        enemiesToKillText.gameObject.SetActive(false);
        timeLeftText.gameObject.SetActive(false);
    }

    public void UpdateCooldown(float _step)
    {
        cooldownImage.fillAmount = _step;
    }



    public void GameOver(bool _won)
    {
        if (_won)
        {
            gameOverText.color = Color.green;
            gameOverText.text = "You won";
        }
        else
        {
            gameOverText.color = Color.red;
            gameOverText.text = "You lost";
        }
        gameOverText.gameObject.SetActive(true);
    }

    public void UpdateAvatars(Sprite _first, Sprite _second, Sprite _third)
    {
        avatar1.sprite = _first;
        avatar2.sprite = _second;
        avatar3.sprite = _third;
    }
}

[System.Serializable]
public class AvatarInfo
{
    public Sprite sprite;
    public int index;
}
