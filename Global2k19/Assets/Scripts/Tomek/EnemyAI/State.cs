﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/State")]
public class State : ScriptableObject
{
    public AIAction[] actions;
    public Transition[] transitions;
    public Color sceneGizmoColor = Color.grey;
    [HideInInspector] public bool ignoreTransision;


    public void UpdateState(EnemyController controller)
    {
        DoActions(controller);

        if (!ignoreTransision)
            CheckTransitions(controller);
    }

    private void DoActions(EnemyController controller)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i].Act(controller);
        }
    }

    private void CheckTransitions(EnemyController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSucceeded = transitions[i].decision.Decide(controller);

            if (decisionSucceeded)
            {
                controller.TransitionToState(transitions[i].trueState);
            }
            else
            {
                controller.TransitionToState(transitions[i].falseState);
            }
        }
    }


}