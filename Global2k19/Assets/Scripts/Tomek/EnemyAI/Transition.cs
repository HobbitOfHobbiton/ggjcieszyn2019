﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Transition
{
    public Decision decision;
    public bool setTrigger = false;
    public State trueState;
    public State falseState;
}