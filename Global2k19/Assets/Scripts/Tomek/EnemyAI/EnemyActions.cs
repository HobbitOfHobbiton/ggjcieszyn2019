﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyActions : MonoBehaviour
{
    public EnemyController controller;
    protected Coroutine actionCoroutine;

    protected bool inAction = false;

    //-------------------------------------------------------------------------------------------


    public bool CheckIfActionEnded()
    {
        return !inAction;
    }


    public virtual void Attack1() { }
    public virtual void Attack2() { }



}
