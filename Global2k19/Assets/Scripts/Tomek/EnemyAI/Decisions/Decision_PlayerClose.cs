﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/PlayerClose")]
public class Decision_PlayerClose : Decision
{
    public LayerMask playerLayer;
    public float distance;

    public override bool Decide(EnemyController _controller)
    {
        Collider[] hitColliders = Physics.OverlapSphere(_controller.transform.position, distance, playerLayer);

        if (hitColliders.Length > 0)
        {
            foreach (var collider in hitColliders)
            {
            }
            return true;
        }
        else
        {
            return false;
        }


    }


}
