﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/PlayerFar")]
public class Decision_PlayerFar : Decision
{
    public LayerMask playerLayer;
    public float distance;

    public override bool Decide(EnemyController _controller)
    {
        Collider[] hitColliders = Physics.OverlapSphere(_controller.transform.position, distance, playerLayer);

        if (hitColliders.Length > 0)
            return false;
        else
            return true;

    }

}
