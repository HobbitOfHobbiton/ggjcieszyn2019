﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/NearDestination")]
public class Decision_NearDestination : Decision {

    public float distance;

    public override bool Decide(EnemyController _controller)
    {
        Debug.Log(_controller.navMeshAgent.remainingDistance);

        if (_controller.navMeshAgent.remainingDistance <= distance && !_controller.navMeshAgent.pathPending)
        {
            Debug.Log("returned true");
            return true;
        }
        else
            return false;
    }
}
