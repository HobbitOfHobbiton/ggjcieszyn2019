﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/EnemyActionEndedDecision")]
public class Decision_EnemyActionEnded : Decision
{
    public override bool Decide(EnemyController _controller)
    {
        return _controller.enemyActions.CheckIfActionEnded();
    }
}
