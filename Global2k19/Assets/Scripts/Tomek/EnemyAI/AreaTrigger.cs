﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTrigger : MonoBehaviour {

    public int stageIndex;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(" something Entered");

        if (other.CompareTag("Enemy") && GameManager.instance.currentStageIndex == stageIndex)
        {
            Debug.Log("Reset Enemy");
            EnemyController enemy = other.GetComponent<EnemyController>();

            enemy.ResetState();
            StartCoroutine(enemy.IgnoreTransision_Cor(3f));
        }
    }


}
