﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{
    public static Vector3 GetVector3(Vector2 _vec)
    {
        return new Vector3(_vec.x, 0f, _vec.y);
    }

    public static Vector2 GetVector2(Vector3 _vec)
    {
        return new Vector2(_vec.x, _vec.z);
    }

    public static IEnumerator ChangeOverTime(System.Action<float> _action, System.Action _endAction, float _duration)
    {
        float time = 0;

        while (true)
        {
            time += Time.deltaTime;
            float step = time / _duration;

            if (time > _duration)
            {
                if (_endAction != null)
                    _endAction.Invoke();
                break;
            }

            if (_action != null)
                _action.Invoke(step);

            yield return null;
        }
    }

    public static bool EvaluateChance(int _chance)
    {
        float value = Random.Range(0, 100);

        if (value <= _chance)
            return true;
        else
            return false;

    }

    public static IEnumerator Disable(GameObject _gameObject, float _time)
    {
        yield return new WaitForSeconds(_time);

        _gameObject.SetActive(false);
    }
}
