﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    [HideInInspector] public EnemyManager enemyManager;
    [HideInInspector] public EnemyActions enemyActions;
    [HideInInspector] public NavMeshAgent navMeshAgent;
    [HideInInspector] public BasicAI basicAI;
    private bool aiActive = false;

    public State defaultState;
    public State currentState;
    public State[] changeStates;

    //public Action[] baseActions;


    [HideInInspector] public List<Transform> wayPointList;
    [HideInInspector] public int nextWayPoint;
    [HideInInspector] public GameObject pickableItemPref;

    [HideInInspector] public bool AIstopped = false;

    //-------------------------------------------------------------------------------------------

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        basicAI = GetComponent<BasicAI>();
    }

    void Update()
    {
        if (!aiActive || AIstopped)
            return;

        currentState.UpdateState(this);

        //for (int i = 0; i < baseActions.Length; i++)
        //{
        //    baseActions[i].Act(this);
        //}
    }

    //-------------------------------------------------------------------------------------------

    public void SetupAI(bool _aiActivationFromEnemyManager, List<Transform> _wayPointsFromTankManager, GameObject _worldSpaceHolder, EnemyManager _manager)
    {
        aiActive = _aiActivationFromEnemyManager;
        enemyManager = _manager;
        wayPointList = _wayPointsFromTankManager;
        currentState = defaultState;
    }


    public void TransitionToState(State _nextState)
    {
        if (_nextState == null)
            return;

        currentState = _nextState;

        ResetTimers();
        DoOnce = true;
    }

    public void ChangeStateState(int _index)
    {
        TransitionToState(changeStates[_index]);
    }

    public void ResetState()
    {
        currentState = defaultState;
    }

    public IEnumerator IgnoreTransision_Cor(float _time)
    {
        currentState.ignoreTransision = true;
        yield return new WaitForSeconds(_time);
        currentState.ignoreTransision = false;
    }

    public void StopAI()
    {
        AIstopped = true;
    }

    //-------------------------------------------------------------------------------------------
    #region Utility
    bool counting = false;
    float time;

    void ResetTimers()
    {
        counting = false;
    }

    public bool WaitForSeconds(float _waitTime)
    {
        if(!counting)
        {
            Debug.Log("Timer Start");
            time = Time.time + _waitTime;
            counting = true;
            return false;
        }

        if(time < Time.time)
        {
            counting = false;
            return true;
        }

        return false;
    }

    bool counting2 = false;
    float time2;

    public bool CheckIfTimeElapsed(float _waitTime, float _variety)
    {
        if (!counting2)
        {
            time2 = Time.time + Random.Range(_waitTime - _variety, _waitTime + _variety);
            counting2 = true;
            return true;
        }

        if (time2 < Time.time)
        {
            counting2 = false;
        }

        return false;
    }


    private bool doOnce = true;

    public bool DoOnce
    {
        get
        {
            if(doOnce)
            {
                DoOnce = false;
                return true;
            }

            return doOnce;
        }

        private set
        {
            doOnce = value;
        }
    }

    IEnumerator SetDoOnceFalse()
    {
        yield return new WaitForEndOfFrame();
        DoOnce = false;
    }
    #endregion
}