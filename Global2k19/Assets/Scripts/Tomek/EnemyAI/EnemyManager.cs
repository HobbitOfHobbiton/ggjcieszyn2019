﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] GameObject worldSpaceHolder;

    public EnemyInfo[] enemyInfos;


    //----------------------------------------------------------------------------------------------------------------------

    private void Start()
    {
        foreach (var enemyInfo in enemyInfos)
        {
            if (enemyInfo.EnemyController == null)
                continue;

            enemyInfo.EnemyController.SetupAI(true, enemyInfo.wayPoints, worldSpaceHolder, this);
        }
    }

    //----------------------------------------------------------------------------------------------------------------------




}

[System.Serializable]
public class EnemyInfo
{
    public EnemyController EnemyController;
    public List<Transform> wayPoints;
}
