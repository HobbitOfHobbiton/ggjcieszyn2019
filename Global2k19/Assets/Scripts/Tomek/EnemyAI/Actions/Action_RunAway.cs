﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/RunAway")]
public class Action_RunAway : AIAction
{
    public float speed;

    public override void Act(EnemyController _controller)
    {
        Vector3 targetPos = _controller.transform.position + (_controller.transform.position - PlayerController.playerGO.transform.position).normalized * 10;
        targetPos[1] = 0f;

        _controller.navMeshAgent.destination = targetPos;

        if (_controller.DoOnce)
        {
            _controller.navMeshAgent.speed = speed;
            _controller.navMeshAgent.isStopped = false;
        }
    }

}
