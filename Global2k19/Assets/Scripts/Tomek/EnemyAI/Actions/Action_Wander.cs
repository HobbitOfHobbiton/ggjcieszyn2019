﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Wander")]
public class Action_Wander : AIAction
{
    public float speed;
    public float minWanderDistance;
    public float maxWanderDistance;
    public float wanderAngle;


    Vector3 pos1 = new Vector3(-171f, -15, 0f);
    Vector3 pos2 = new Vector3(-168f, -15, 0f);

    public override void Act(EnemyController _controller)
    {
        Wander(_controller);
    }

    private void Wander(EnemyController _controller)
    {
        if (_controller.DoOnce)
        {
            _controller.navMeshAgent.speed = speed;
            _controller.navMeshAgent.isStopped = false;

            Vector2 vec = Utility.GetVector2(_controller.transform.position - PlayerController.playerGO.transform.position);
            float phi = Mathf.Atan2(vec.y, vec.x);
            phi = Random.Range(phi - wanderAngle / 2 * Mathf.Deg2Rad, phi + wanderAngle / 2 * Mathf.Deg2Rad);

            float distance = Random.Range(minWanderDistance, maxWanderDistance);
            Vector3 newPos = PlayerController.playerGO.transform.position + Utility.GetVector3(new Vector2(distance * Mathf.Cos(phi), distance * Mathf.Sin(phi)));
            Debug.Log(newPos);

            _controller.navMeshAgent.destination = newPos;
        }


        if (_controller.navMeshAgent.remainingDistance <= _controller.navMeshAgent.stoppingDistance && !_controller.navMeshAgent.pathPending)
        {
            Vector2 vec = Utility.GetVector2(_controller.transform.position - PlayerController.playerGO.transform.position);
            float phi = Mathf.Atan2(vec.y, vec.x);
            phi = Random.Range(phi - wanderAngle / 2 * Mathf.Deg2Rad, phi + wanderAngle / 2 * Mathf.Deg2Rad);

            float distance = Random.Range(minWanderDistance, maxWanderDistance);
            Vector3 newPos = PlayerController.playerGO.transform.position + Utility.GetVector3(new Vector2(distance * Mathf.Cos(phi), distance * Mathf.Sin(phi)));

            _controller.navMeshAgent.destination = newPos;

        }

    }
}