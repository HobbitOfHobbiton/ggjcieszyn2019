﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Chase")]
public class Action_Chase : AIAction
{
    public float speed;

    public override void Act(EnemyController _controller)
    {
        Chase(_controller);
    }

    private void Chase(EnemyController _controller)
    {
        if (_controller.DoOnce)
        {
            _controller.navMeshAgent.speed = speed;
            _controller.navMeshAgent.isStopped = false;
        }

        _controller.navMeshAgent.destination = PlayerController.playerGO.transform.position;
    }
}