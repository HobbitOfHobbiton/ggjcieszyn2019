﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Action_Attack1")]
public class Action_Attack2 : AIAction
{
    public override void Act(EnemyController _controller)
    {
        Attack(_controller);
    }

    private void Attack(EnemyController _controller)
    {
        if (_controller.DoOnce)
        {
            _controller.navMeshAgent.enabled = true;
            _controller.navMeshAgent.isStopped = true;
            _controller.enemyActions.Attack2();
        }

    }
}
