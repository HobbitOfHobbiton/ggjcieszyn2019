﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Patrol")]
public class Action_Patrol : AIAction
{
    public float speed;

    public override void Act(EnemyController _controller)
    {
        Patrol(_controller);
    }

    private void Patrol(EnemyController _controller)
    {
        if (_controller.wayPointList.Count == 0)
            return;

        _controller.basicAI.SetDestination(_controller.wayPointList[_controller.nextWayPoint].position);

        if (_controller.navMeshAgent.remainingDistance <= _controller.navMeshAgent.stoppingDistance && !_controller.navMeshAgent.pathPending)
        {
            _controller.nextWayPoint = (_controller.nextWayPoint + 1) % _controller.wayPointList.Count;
        }

        if (_controller.DoOnce)
        {
            _controller.navMeshAgent.speed = speed;
            _controller.navMeshAgent.isStopped = false;
        }
    }
}