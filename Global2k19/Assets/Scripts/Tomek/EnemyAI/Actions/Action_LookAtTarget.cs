﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/LookAtTarget")]
public class Action_LookAtTarget: AIAction
{
    public float speed;

    public override void Act(EnemyController _controller)
    {
        _controller.basicAI.SetLookPos(PlayerController.playerGO.transform);
    }

}
