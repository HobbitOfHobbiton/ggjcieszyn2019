﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Stop")]
public class Action_Stop : AIAction
{
    public override void Act(EnemyController controller)
    {
        Patrol(controller);
    }

    private void Patrol(EnemyController _controller)
    {
        if (_controller.DoOnce)
        {
            _controller.navMeshAgent.enabled = true;
            _controller.navMeshAgent.isStopped = true;
        }

        //if (_controller.transform.position.x > PlayerController.playerGO.transform.position.x)
        //{
        //    _controller.transform.GetChild(0).transform.GetChild(0).transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        //}
        //else if (_controller.transform.position.x < PlayerController.playerGO.transform.position.x)
        //    _controller.transform.GetChild(0).transform.GetChild(0).transform.localEulerAngles = new Vector3(0f, 180f, 0f);
    }
}
