﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {

    [SerializeField] RectTransform healthBarRect;

    void Update()
    {
        transform.LookAt(Camera.main.transform);
    }

    public void ScaleHealthBar(float _currentH, float _maxH)
    {
        float ratio = _currentH / _maxH;

        healthBarRect.localScale = new Vector3(ratio, 1f, 1f);
    }


}
