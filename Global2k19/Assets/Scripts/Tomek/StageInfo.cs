﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "StageInfo")]
public class StageInfo : ScriptableObject {

    public Goal goal = Goal.KillEnemies;
    public int enemiesTokillCount;
    public float timerDuration;

}
