﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerFollow : MonoBehaviour {

    public List<NavMeshAgent> players = new List<NavMeshAgent>();
    int currentPlayerIndex;

    public Transform target;

    private void Start()
    {
        int priority = 1;

        foreach (var player in players)
        {
            player.avoidancePriority = priority;
            priority++;
        }
    }

    private void Update()
    {
        for (int i = 0; i < players.Count - 1; i++)
        {
            Vector3 nextPlayerPos = -players[i].transform.forward + players[i].transform.position;
              
            players[i + 1].SetDestination(nextPlayerPos);
        }

    }


    public void RemoveCharacter(NavMeshAgent _agent)
    {
        players.Remove(_agent);
    }


    public void ChangePlayer(NavMeshAgent _newPlayer)
    {
        players.Remove(_newPlayer);
        players.Insert(0 ,_newPlayer);

        int priority = 1;

        foreach (var player in players)
        {
            player.avoidancePriority = priority;
            priority++;
        }

        GameManager.instance.gameUI.UpdateAvatars(players[0].GetComponent<BasicAI>().avatar, players[1].GetComponent<BasicAI>().avatar, players[2].GetComponent<BasicAI>().avatar);

    }



}
