﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] Image fillImage;
    [SerializeField] float fillSmoothing = 5.0f;

    Transform followTarget;
    float targetFill = 1.0f;

    private void Update()
    {
        fillImage.fillAmount = Mathf.Lerp(fillImage.fillAmount, targetFill, Time.deltaTime * fillSmoothing);
        transform.position = new Vector3(followTarget.position.x, followTarget.position.y + 0.1f, followTarget.position.z);
    }

    public void Init(Transform target)
    {
        followTarget = target;
    }

    public void UpdateFill(float fillAmount)
    {
        targetFill = fillAmount;
    }

}
