﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public bool Dead { get { return dead; } }

    [SerializeField] float maxHealth = 100.0f;
    [SerializeField] HealthBar healthBarPrefab;

    CapsuleCollider capsuleCollider;
    Animator animator;

    HealthBar healthBarInstance;
    float currentHealth;
    bool dead;

    Rigidbody[] bones;

	void Start ()
    {
        capsuleCollider = GetComponent<CapsuleCollider>();
        bones = GetComponentsInChildren<Rigidbody>();
        ToggleBones(false);

        animator = GetComponent<Animator>();

        currentHealth = maxHealth;
        healthBarInstance = Instantiate(healthBarPrefab);
        healthBarInstance.Init(transform);
	}

    public void TakeHit(float damage)
    {
        currentHealth -= damage;
        healthBarInstance.UpdateFill(currentHealth / maxHealth);

        if(currentHealth <= 0)
        {
            OnDeath();
            return;
        }

        GetComponent<Animator>().SetTrigger("TakeHit");
    }

    void OnDeath()
    {
        dead = true;
        GetComponent<BasicAI>().Disable();
        capsuleCollider.enabled = false;
        animator.enabled = false;
        ToggleBones(true);

        Destroy(healthBarInstance.gameObject);
        GameManager.instance.KilledEnemy();
    }

    void ToggleBones(bool toggle)
    {
        foreach(var bone in bones)
        {
            bone.isKinematic = !toggle;
            bone.detectCollisions = toggle;
        }
    }
}
