﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private float _cameraSmoothing = 0.5f;

    PlayerController playerController;

	void Start ()
    {
        playerController = FindObjectOfType<PlayerController>();
	}
	
	void Update ()
    {
        _target = playerController.CurrentCharacter.transform;

        transform.position = Vector3.Lerp(transform.position, _target.position, Time.deltaTime * _cameraSmoothing);
        //Camera.main.transform.LookAt(_target);
	}
}
