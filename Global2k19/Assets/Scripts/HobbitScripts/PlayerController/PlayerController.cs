﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    public static GameObject playerGO;
    PlayerFollow playerFollow;
    public LayerMask enemyLayer;

    public BasicAI CurrentCharacter { get { return currentCharacter; } }

    [SerializeField] Transform target;
    [SerializeField] List<BasicAI> characters;
    [SerializeField] SpriteRenderer rangeRender;
    [SerializeField] Color inRange, outRange;

    BasicAI previousBasicAI = null;

    BasicAI currentCharacter;
    int characterIndex;
    bool hasTarget;

	void Start ()
    {
        currentCharacter = characters[characterIndex];
        currentCharacter.controlledByPlayer = true;
        playerFollow = GetComponentInParent<PlayerFollow>();
        playerGO = currentCharacter.gameObject;
    }
	
	void Update ()
    {
        if (currentCharacter == null)
            return;

        if (Input.GetMouseButton(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                target.position = hit.point;
                currentCharacter.SetDestination(target.position);
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100f, enemyLayer))
            {
                if (hit.transform.CompareTag("Interactable"))
                {
                    if (previousBasicAI != null)
                        previousBasicAI.ToggleTarget(false);

                    Debug.Log("Targetting enemy");
                    currentCharacter.SetLookPos(hit.transform);
                    hasTarget = true;
                    previousBasicAI = hit.transform.GetComponent<BasicAI>();
                    previousBasicAI.ToggleTarget(true);
                }
            }
            else
            {
                Debug.Log("No targetting");
                currentCharacter.SetLookPos(null);
                hasTarget = false;


                if (previousBasicAI != null)
                    previousBasicAI.ToggleTarget(false);
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentCharacter.controlledByPlayer = false;
            characterIndex = 0;
            currentCharacter = characters[characterIndex];
            currentCharacter.controlledByPlayer = true;
            playerFollow.ChangePlayer(currentCharacter.GetComponent<NavMeshAgent>());
            playerGO = currentCharacter.gameObject;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentCharacter.controlledByPlayer = false;
            characterIndex = 1;
            currentCharacter = characters[characterIndex];
            currentCharacter.controlledByPlayer = true;
            playerFollow.ChangePlayer(currentCharacter.GetComponent<NavMeshAgent>());
            playerGO = currentCharacter.gameObject;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            currentCharacter.controlledByPlayer = false;
            characterIndex = 2;
            currentCharacter = characters[characterIndex];
            currentCharacter.controlledByPlayer = true;
            playerFollow.ChangePlayer(currentCharacter.GetComponent<NavMeshAgent>());
            playerGO = currentCharacter.gameObject;
        }

        RangeRenderer();
    }

    public void ChangeToNextCharacter()
    {
        currentCharacter.controlledByPlayer = false;
        characterIndex++;

        if(characterIndex >= characters.Count)
        {
            characterIndex = 0;
        }

        currentCharacter = characters[characterIndex];
        currentCharacter.controlledByPlayer = true;
        playerFollow.ChangePlayer(currentCharacter.GetComponent<NavMeshAgent>());
        playerGO = currentCharacter.gameObject;
    }

    void RangeRenderer()
    {
        rangeRender.transform.position = currentCharacter.transform.position;
        if (hasTarget)
        {
            rangeRender.enabled = true;
            float factor = currentCharacter.Range * 2;
            rangeRender.transform.localScale = new Vector3(factor, factor, factor);
            if (currentCharacter.InRange)
                rangeRender.color = inRange;
            else
                rangeRender.color = outRange;
        }
        else
        {
            rangeRender.enabled = false;
        }
    }
}
