﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class Weapon : MonoBehaviour
{
    public Transform MuzzlePoint { get { return muzzlePoint; } }

    [SerializeField] Transform muzzlePoint;
    [SerializeField] ParticleSystem particles;

    StudioEventEmitter emitter;

	void Start ()
    {
        emitter = GetComponent<StudioEventEmitter>();
	}
	
    public void UseWeapon(Skill skill, Transform target)
    {
        particles.transform.LookAt(target);
        particles.Emit(1);

        if (emitter != null)
        {
            emitter.Play();
        }
    }
}
