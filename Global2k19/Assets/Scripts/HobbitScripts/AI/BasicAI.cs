﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class BasicAI : MonoBehaviour
{
    public Weapon Weapon { get { return weapon; } }
    public float Range { get { return range; } }

    [SerializeField] float lookRotSlerpSpeed = 5.0f;
    [SerializeField] float dampTime = 0.1f;
    [SerializeField] protected float range = 5.0f;
    [SerializeField] Skill[] skills;

    Transform lookTransform;

    EnemyController enemyController;
    PlayerController playerController;
    NavMeshAgent agent;
    Animator animator;
    Weapon weapon;

    public GameObject targetGO;

    public Sprite avatar;

    public bool controlledByPlayer;
    public bool disabled;
    public bool InRange
    {
        get
        {
            if (!lookTransform)
                return false;
            else
            {
                if(Vector3.Distance(lookTransform.position, transform.position) <= range)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
    

	void Start ()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        weapon = GetComponentInChildren<Weapon>();
        enemyController = GetComponent<EnemyController>();

        playerController = transform.parent.GetComponentInChildren<PlayerController>();
    }
	
	void Update ()
    {
        UpdateAnimator();
        UpdateLook();
        UpdateSkills();
	}

    void UpdateAnimator()
    {
        Vector3 localVelocity = transform.InverseTransformDirection(agent.velocity);
        float angle = Vector3.SignedAngle(agent.velocity, agent.desiredVelocity, transform.up);

        animator.SetFloat("Vertical", localVelocity.z, dampTime, Time.deltaTime);
        animator.SetFloat("Horizontal", localVelocity.x, dampTime, Time.deltaTime);
        animator.SetFloat("Angle", angle, dampTime, Time.deltaTime);
    }

    void UpdateLook()
    {
        if (lookTransform)
        {
            Vector3 lookRot = Quaternion.LookRotation(lookTransform.position - transform.position, transform.up).eulerAngles;
            lookRot = new Vector3(0, lookRot.y, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(lookRot), Time.deltaTime * lookRotSlerpSpeed);
        }
    }

    void UpdateSkills()
    {
        if (controlledByPlayer)
        {
            if (lookTransform != null)
            {
                for (int i = 0; i < skills.Length; i++)
                {
                    if (Input.GetKeyDown(skills[i].SkillKey) && lookTransform.GetComponent<Health>() != null)
                    {
                        if (!lookTransform.GetComponent<Health>().Dead && !cooldownOn)
                        {
                            weapon.UseWeapon(skills[i], lookTransform);
                            skills[i].Use(this, lookTransform.GetComponent<Health>());
                            StartCoroutine(Cooldown_Cor(skills[i].Cooldown));
                        }
                    }
                }
            }
        }
    }

    bool cooldownOn = false;

    IEnumerator Cooldown_Cor(float _cooldown)
    {
        cooldownOn = true;

        for (float time = 0; time < _cooldown; time += Time.deltaTime)
        {
            float step = time / _cooldown;
            GameManager.instance.gameUI.UpdateCooldown(step);

            yield return null;
        }

        GameManager.instance.gameUI.UpdateCooldown(1f);

        cooldownOn = false;
    }

    public void ToggleTarget(bool _activate)
    {
        targetGO.SetActive(_activate);
    }

    public void UseEnemySkill()
    {
        skills[0].Use(this, lookTransform.GetComponent<Health>());
    }

    public void Disable()
    {
        if (controlledByPlayer)
        {
            playerController.ChangeToNextCharacter();
            PlayerFollow playerFollow = GetComponentInParent<PlayerFollow>();

            if (playerFollow != null)
                playerFollow.RemoveCharacter(agent);


        }

        agent.SetDestination(transform.position);
        agent.isStopped = true;
        agent.enabled = false;
        disabled = true;

        if (enemyController != null)
        {
            PlayerController.playerGO.transform.parent.GetComponentInChildren<PlayerController>().CurrentCharacter.SetLookPos(null);
            enemyController.StopAI();
            ToggleTarget(false);
        }

        this.enabled = false;
    }

    public void SetDestination(Vector3 destination)
    {
        agent.SetDestination(destination);
    }

    public void SetLookPos(Transform target)
    {
        lookTransform = target;
    }
}
