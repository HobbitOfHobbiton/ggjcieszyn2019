﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Skill : ScriptableObject
{
    public KeyCode SkillKey { get { return skillKey; } }
    public float Damage { get { return damage; } }
    public float Cooldown { get { return cooldown; } }

    public string AnimatorTrigger { get { return animatorTrigger; } }
    public AudioClip AudioClip { get { return audioClip; } }

    [SerializeField] protected KeyCode skillKey;
    [SerializeField] protected float damage = 10;
    [SerializeField] protected float cooldown = 0.1f;
    [SerializeField] protected string animatorTrigger = "Attack";
    [SerializeField] protected AudioClip audioClip;

    public abstract void Use(BasicAI caster, Health target);
}
