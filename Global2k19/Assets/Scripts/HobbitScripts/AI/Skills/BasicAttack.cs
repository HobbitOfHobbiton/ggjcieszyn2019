﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Basic Attack", menuName = "Skills/Basic Attack")]
public class BasicAttack : Skill
{
    [SerializeField] protected GameObject hitFX;

    public override void Use(BasicAI caster, Health target)
    {
        if (caster.InRange)
        {
            RaycastHit hit;


            Debug.Log(caster.Weapon.MuzzlePoint.position);
            Debug.Log(target.transform.position - caster.Weapon.MuzzlePoint.position);


            if (Physics.Raycast(caster.Weapon.MuzzlePoint.position, target.transform.position - caster.Weapon.MuzzlePoint.position, out hit))
            {
                if (hit.transform.GetComponentInParent<Health>() == target)
                {
                    caster.transform.GetComponent<Animator>().SetTrigger(animatorTrigger);
                    target.TakeHit(damage);
                }
            }
        }
    }
}
